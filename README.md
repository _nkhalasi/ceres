# README #

The goal of this readme is to get you started with setting up your development environment for working with **ceres** project. 
**ceres** is built using multi-tenant architecture. The **platform** subproject provides the tenant and tenant database management services.

1. Manage the platform database that holds the metadata of all the tenant setups (mainly the tenant databases)
1. Manage the provisioning and deprovisioning of tenants.

### How do I get set up? ###

* Gearing up for development
> __Pre-requisites__
>
> 1. JDK 7+
> 1. SBT 0.13+
> 1. Scala IDE (I use Sublime Text 3)
> 1. MySQL database
> 1. [Jasypt](http://www.jasypt.org/) - We use this library to add encryption/decryption support with minimal efforts. Especially we will use its `./encrypt.sh` command documented in its [cli](http://www.jasypt.org/cli.html) section for generating the initial encrypted password for the platform database which will be configured in `platform_datasource.properties` file.
>
> __Clone the repository__ 
>
> `git clone git@bitbucket.org:_nkhalasi/ceres.git`
>
> __Initial Configuration__
>
> 1. Create the platform database and DB admin user.
>> `mysql -u root -p mysql`
>>
>> `create database platform`
>>
>> `grant all on *.* to 'pfadmin'@'localhost' identified by 'password_of_your_choice' with grant option`
>>
>> ctrl+d
>>
>> Verify that the user can access the database `mysql -u pfadmin -p platform`
> 1. The key used for encrypting the platform database admin password has been configured in `project/Build.scala`. Look for `-Dplatform.datasource.password_enckey=`. You may change this but for better security try to keep it long and containing random alphanumeric characters.
> 1. Use the jasypt's `./encrypt.sh` command to generate an encrypted string for `password_of_your_choice` above
> 1. Configure `platform/src/main/resources/platform_datasource.properties` appropriately (especially the encrypted password)
>
> __Compiling and Running__
>
> 1. Start `sbt` from your project root directory
> 1. Run `compile` command to compile your sources
> 1. Run `platform/run-main com.ceres.platform.db.ManagePlatformDatabase create` to create the tables in the `platform` database
> 1. Run `platform/run-main com.ceres.platform.db.ManagePlatformDatabase reset` to drop the tables in the `platform` database
> 1. Run `platform/run-main com.ceres.platform.db.ManageTenants provision --tenant <tenant-name> --tenant-db-admin <tenant-admin-user> --tenant-db-password <tenant-admin-password>` to provision a new tenant
> 1. Run `platform/run-main com.ceres.platform.db.ManageTenants deprovision --tenant <tenant-name>` to deprovision an existing tenant