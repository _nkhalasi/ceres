package com.ceres.platform.db

import org.rogach.scallop._
object ManagePlatformDatabase {
    class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
        val createDbCmd = new Subcommand("create")
        val resetDbCmd = new Subcommand("reset")
    }

    def main(args: Array[String]) {
        val cmd = new Conf(args)
        cmd.subcommand match {
            case Some(cmd.resetDbCmd) => PlatformManager.platformManager.reset()
            case Some(cmd.createDbCmd) => PlatformManager.platformManager.create()
            case default => println("Invalid Option")
        }
    }
}///:~

object ManageTenants {
    class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
        val provisionCmd = new Subcommand("provision") {
            val tenant = opt[String](required = true)
            val tenantDbAdmin = opt[String](required = true)
            val tenantDbPassword = opt[String](required = true)
        }
        val deprovisionCmd = new Subcommand("deprovision") {
            val tenant = opt[String](required = true)
        }
    }

    def main(args: Array[String]) {
        val cmd = new Conf(args)
        cmd.subcommand match {
            case Some(cmd.provisionCmd) => TenantManager.tenantManager.provisionTenant(cmd.provisionCmd.tenant(), 
                                                                                        cmd.provisionCmd.tenantDbAdmin(),
                                                                                        cmd.provisionCmd.tenantDbPassword())
            case Some(cmd.deprovisionCmd) => TenantManager.tenantManager.deprovisionTenant(cmd.deprovisionCmd.tenant())
            case default => println("Invalid Option")
        }
    }
}///:~