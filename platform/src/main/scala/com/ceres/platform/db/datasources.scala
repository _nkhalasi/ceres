package com.ceres.platform.db

import scala.slick.jdbc.JdbcBackend.Database
import scala.slick.driver.MySQLDriver.simple._

case class Tenant(id: Option[Long], name: String, description: Option[String], tenantUUID: String, active: Boolean, originalName: Option[String])

class Tenants(tag: Tag) extends Table[Tenant](tag, "tenants") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.DBType("VARCHAR(100)"))
    def description = column[String]("description", O.DBType("VARCHAR(1024)"), O.Nullable)
    def tenantUUID = column[String]("tenant_id", O.DBType("VARCHAR(100)"))
    def active = column[Boolean]("is_active", O.DBType("TINYINT"), O.Default(true))
    def originalName = column[String]("original_name", O.DBType("VARCHAR(100)"), O.Nullable)
    def * = (id.?, name, description.?, tenantUUID, active, originalName.?) <> (Tenant.tupled, Tenant.unapply)
    def nameIdx = index("tenant_name_idx", name, unique=true)
    def tenantIdIdx = index("tenant_id_idx", tenantUUID, unique=true)
}///:~

object tenants extends TableQuery(new Tenants(_))

import com.ceres.platform.db.encryptutils.DBPasswordEncryptor
sealed trait TenantDatabase {
    val id: Option[Long]
    val tenantId: Long
    val driverClass: String
    val jdbcUrl: String
    val dbUser: String
    val dbPassword: String
    val dbHost: String
    val dbPort: Int
    def actualDbPassword = {
        val decryptedPassword = DBPasswordEncryptor.decrypt(dbPassword.drop(20))
        decryptedPassword.drop(17).reverse.drop(13).reverse
    }
}///:~

object TenantDatabase {
    //Inspired by http://www.jjask.com/190461/factory-object-and-case-class
    private def salt(length: Int) = scala.util.Random.alphanumeric.take(length).mkString

    def apply(id: Option[Long], tenantId: Long, driverClass: String, jdbcUrl: String, dbUser: String, dbPassword: String, dbHost: String, dbPort:Int): TenantDatabase = {
        val encryptedDbPassword = salt(20) + DBPasswordEncryptor.encrypt(salt(17)+dbPassword+salt(13))
        new Value(id, tenantId, driverClass, jdbcUrl, dbUser, encryptedDbPassword, dbHost, dbPort) with TenantDatabase
    }

    def unapply(td: TenantDatabase) = {
        Some(td.id, td.tenantId, td.driverClass, td.jdbcUrl, td.dbUser, td.dbPassword, td.dbHost, td.dbPort)
    }
    
    case class Value[TenantDatabase](id: Option[Long], tenantId: Long, driverClass: String, jdbcUrl: String, dbUser: String, dbPassword: String, dbHost: String, dbPort: Int)
}///:~

//http://stackoverflow.com/questions/15627981/mapped-projection-with-companion-object-in-slick
//http://stackoverflow.com/questions/15175659/mapped-projection-with-to-a-case-class-with-companion-object-in-slick
class TenantDatabases(tag: Tag) extends Table[TenantDatabase](tag, "tenant_databases") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def tenantId = column[Long]("tenant_id")
    def tenant = foreignKey("tenant_fk", tenantId, tenants)(_.id, onUpdate=ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Cascade)
    def driverClass = column[String]("driver_class", O.DBType("VARCHAR(256)"))
    def jdbcUrl = column[String]("jdbc_url", O.DBType("VARCHAR(256)"))
    def dbUser = column[String]("db_username", O.DBType("VARCHAR(30)"))
    def dbPassword = column[String]("db_password", O.DBType("VARCHAR(512)"))
    def dbHost = column[String]("db_host", O.DBType("VARCHAR(256)"))
    def dbPort = column[Int]("db_port", O.DBType("INTEGER"))
    def * = (id.?, tenantId, driverClass, jdbcUrl, dbUser, dbPassword, dbHost, dbPort) <> ((TenantDatabase.apply _).tupled, TenantDatabase.unapply)
}///:~

object tenantDatabases extends TableQuery(new TenantDatabases(_))

trait DatasourceBuilder {
    private val bonecp = 
        (driver:String, jdbcurl:String, dbuser:String, dbpwd: String) => {
            import com.jolbox.bonecp.BoneCPDataSource
            val ds = new BoneCPDataSource()
            ds.setDriverClass(driver)
            ds.setJdbcUrl(jdbcurl)
            ds.setUsername(dbuser)
            ds.setPassword(dbpwd)
            Database.forDataSource(ds)
        }
    private val c3p0cp = 
        (driverClass: String, jdbcUrl: String, dbUser: String, dbPassword: String) => {
            import com.mchange.v2.c3p0.{ComboPooledDataSource}
            val ds = new ComboPooledDataSource()
            ds.setDriverClass(driverClass)
            ds.setJdbcUrl(jdbcUrl)
            ds.setUser(dbUser)
            ds.setPassword(dbPassword)
            Database.forDataSource(ds)
        }
    def datasourceFor(driver: String, jdbcurl: String, dbuser: String, dbpwd: String) = c3p0cp(driver, jdbcurl, dbuser, dbpwd)
}///:~

trait DatasourceConfig {
    private val dsProps  = scala.collection.immutable.Map(
            "driverclass"   -> "platform.datasource.driverclass",
            "jdbcurl"       -> "platform.datasource.jdbcurl",
            "username"      -> "platform.datasource.username",
            "password"      -> "platform.datasource.password",
            "dbhost"        -> "platform.datasource.dbhost",
            "dbport"        -> "platform.datasource.dbport"
        )
    def dsConfig(configFile: String)(prop: String) = {
        import com.ceres.platform.db.encryptutils.EncryptablePropertiesReader
        val config = EncryptablePropertiesReader(configFile)
        config.getProperty(dsProps(prop))
    }
}///:~

trait PlatformDatasource extends DatasourceConfig with DatasourceBuilder {
    val platformDsConfig = dsConfig("platform_datasource.properties") _
    val platformDs = datasourceFor(platformDsConfig("driverclass"), platformDsConfig("jdbcurl"), platformDsConfig("username"), platformDsConfig("password"))
}///:~

trait PlatformManagerComponent {
    this: PlatformDatasource =>
    val tables = Seq(tenants, tenantDatabases)
    val platformManager: PlatformManagerService

    import scala.slick.jdbc.meta.MTable
    class PlatformManagerService {
        private def _tableNames(implicit session: Session) = MTable.getTables.list.map(_.name.name)

        def create() = platformDs withSession {
            implicit session =>
            tables.filterNot(t => _tableNames.contains(t.baseTableRow.tableName)).foreach(t => t.ddl.create)
        }

        def reset() = platformDs withSession {
            implicit session =>
            tables.reverse.filter(t => _tableNames.contains(t.baseTableRow.tableName)).foreach(t => t.ddl.drop)
        }        
    }
}///:~

//http://stackoverflow.com/questions/13661339/slick-how-to-write-db-agnostic-apps-and-perform-first-time-db-initialization
//http://grokbase.com/t/gg/scalaquery/1426z3ra3r/using-slick-in-a-db-agnostic-fashion-without-the-cake-pattern
//https://groups.google.com/forum/#!msg/scalaquery/zJxXA83mITM/MUahsFtLHh0J

//TODO: Tenant Admin user created but create user should be "grant all on dbname.* to user@<app servers>". Similar handling for drop users.
class TenantAwareCache[T] {
    private val cache = scala.collection.mutable.Map[String, T]()

    def get(tenantUUID: String) = if (cache.contains(tenantUUID)) Some(cache(tenantUUID)) else None
    def add(tenantUUID: String, entity: T) = cache(tenantUUID) = entity
    def remove(tenantUUID: String) = if (cache.contains(tenantUUID)) cache - tenantUUID
}

trait TenantAwareCacheComponent {
    this: PlatformDatasource =>
    val tenantAwareCaches: TenantAwareCaches

    class TenantAwareCaches {
        val tenantDataSources = new TenantAwareCache[Database]()
        val tenantDatabaseConfigs = new TenantAwareCache[TenantDatabase]()
        //load datasources for all active tenants
        platformDs withSession {
            implicit session =>
            val tdList = (for {
                t <- tenants
                td <- tenantDatabases if (td.tenantId === t.id && t.active === true)
            } yield(t, td)).list
            tdList.foreach(ttd => tenantDataSources.add(ttd._1.tenantUUID, datasourceFor(ttd._2.driverClass, ttd._2.jdbcUrl, ttd._2.dbUser, ttd._2.actualDbPassword)))
            tdList.foreach(ttd => tenantDatabaseConfigs.add(ttd._1.tenantUUID, ttd._2))
        }
    }
}///:~

trait TenantManagerComponent {
    this: TenantAwareCacheComponent with PlatformDatasource =>
    val tenantManager: TenantManagerService

    import java.text.SimpleDateFormat
    import java.util.Calendar
    import java.util.UUID
    import scala.slick.jdbc.{StaticQuery => Q}
    class TenantManagerService {        
        def provisionTenant(tenantName: String, adminUser: String, adminPassword: String) = platformDs withSession {
            implicit session =>
            val tenant = try {
                Some(tenants.filter(t => t.name === tenantName && t.active === true).first)
            } catch {
                case e: NoSuchElementException => None
            }

            tenant match {
                case Some(t) => 
                    tenantAwareCaches.tenantDataSources.get(t.tenantUUID) match {
                        case Some(td) => println("Tenant already provisioned")
                        case None => println("Something wrong...tenant found but tenant database not found in cache")
                    }
                case None =>
                    val newTenantUUID = UUID.randomUUID.toString
                    val newTenantId = (tenants returning tenants.map(_.id)) += Tenant(None, tenantName, None, newTenantUUID, true, None)
                    val td = TenantDatabase(None, newTenantId, platformDsConfig("driverclass"), platformDsConfig("jdbcurl"),
                                            adminUser, adminPassword, platformDsConfig("dbhost"), platformDsConfig("dbport").toInt) 
                    tenantDatabases += td
                    tenantAwareCaches.tenantDatabaseConfigs.add(newTenantUUID, td)
                    _createTenantDatabase(tenantName, adminUser, adminPassword)
                    tenantAwareCaches.tenantDataSources.add(newTenantUUID, datasourceFor(platformDsConfig("driverclass"), 
                                                                                platformDsConfig("jdbcurl"), adminUser, adminPassword))
            }
        }

        def deprovisionTenant(tenantName: String) = platformDs withSession {
            implicit session =>
            val tenant = try {
                Some(tenants.filter(t => t.name === tenantName && t.active === true).first)
            } catch {
                case e: NoSuchElementException => None
            }
            tenant match {
                case Some(t) => {
                    val today = new SimpleDateFormat("yyyyMMdd HHmmss").format(Calendar.getInstance().getTime())
                    val q = for {t1 <- tenants if (t1.name === tenantName && t1.active === true)} yield (t1.name, t1.active, t1.originalName)
                    q.update((tenantName.take(80) + " dropped on " + today, false, tenantName))
                    _dropTenantDatabase(tenantName, tenantAwareCaches.tenantDatabaseConfigs.get(t.tenantUUID).get.dbUser)
                    tenantAwareCaches.tenantDataSources.remove(t.tenantUUID)
                    tenantAwareCaches.tenantDatabaseConfigs.remove(t.tenantUUID)
                }
                case None => println("Tenant not found for deprovisioning")
            }
        }

        private def _createTenantDatabase(tenantName: String, adminUser: String, adminPassword: String)(implicit session: Session) = {
            Q.updateNA(f"create database $tenantName%s_db").execute
            Q.updateNA(f"grant all on $tenantName%s_db.* to '$adminUser'@'localhost' identified by '$adminPassword'").execute
        }

        private def _dropTenantDatabase(tenantName: String, adminUser: String)(implicit session: Session) = {
            Q.updateNA(f"drop database $tenantName%s_db").execute
            Q.updateNA(f"drop user '$adminUser'@'localhost'").execute
        }
    }
}///:~

object PlatformManager extends PlatformManagerComponent with PlatformDatasource {
    val platformManager = new PlatformManagerService
}///:~

object TenantManager extends TenantManagerComponent with TenantAwareCacheComponent with PlatformDatasource {
    val tenantAwareCaches = new TenantAwareCaches
    val tenantManager = new TenantManagerService
}///:~   