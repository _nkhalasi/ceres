package com.ceres.platform.db.encryptutils

import org.jasypt.properties.EncryptableProperties
import org.jasypt.encryption.pbe.config.{EnvironmentStringPBEConfig}
import org.jasypt.encryption.pbe.{StandardPBEStringEncryptor}

trait Encryptor {
    val encryptor = {
        val pbeConfig = new EnvironmentStringPBEConfig
        pbeConfig.setAlgorithm("PBEWithMD5AndTripleDES")
        pbeConfig.setPasswordSysPropertyName("platform.datasource.password_enckey")
        val encryptor = new StandardPBEStringEncryptor
        encryptor.setPassword(pbeConfig.getPassword())
        encryptor
    }
    def encrypt(data: String) = encryptor.encrypt(data)
    def decrypt(encryptedData: String) = encryptor.decrypt(encryptedData)
}///:~

object EncryptablePropertiesReader extends Encryptor {
    def apply(resource: String) = {
        val props = new EncryptableProperties(encryptor)
        props.load(getClass.getClassLoader.getResourceAsStream(resource))
        props
    }
}///:~

object DBPasswordEncryptor extends Encryptor