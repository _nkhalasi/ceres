trait Friendly {
	def greet() = "Hi"
}

class Dog extends Friendly {
	override def greet() = "Woof"
}

class HungryCat extends Friendly {
	override def greet() = "Meow"
}

class HungryDog extends Friendly {
	override def greet() = "I'd like to eat my own dog food"
}

trait ExclaimatoryGreeter extends Friendly {
	override def greet() = super.greet() + "!"
}

object FriendlyApp {
	def main(args: Array[String]) = {
		var pet:Friendly = new Dog
		println(pet.greet())

		pet = new HungryCat
		println(pet.greet())

		pet = new HungryDog
		println(pet.greet())

		pet = new Dog with ExclaimatoryGreeter
		println(pet.greet())

		pet = new HungryCat with ExclaimatoryGreeter
		println(pet.greet())

		pet = new HungryDog with ExclaimatoryGreeter
		println(pet.greet())		
	}
}
