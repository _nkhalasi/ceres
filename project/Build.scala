import sbt._
import Keys._

object CeresBuild extends Build {
    lazy val defaultSettings = Defaults.defaultSettings ++ Seq (
            version := "1.0-SNAPSHOT",
            scalaVersion := "2.11.6",
            libraryDependencies ++= Seq(
                    "org.rogach" %% "scallop" % "0.9.5",
                    "org.jasypt" % "jasypt" % "1.9.2",
                    "com.typesafe" % "config" % "1.2.1",
                    "org.specs2" %% "specs2" % "2.4.2",
                    "org.scalacheck" %% "scalacheck" % "1.11.5"
                )
        )
    lazy val platform = Project("platform", file("platform"), 
            settings=defaultSettings ++ Seq(
                fork := true,
                javaOptions := Seq("-Dplatform.datasource.password_enckey=SRLWR8GthPGBaXNqH9kahhKd"),
                libraryDependencies ++= Seq(
                    "mysql" % "mysql-connector-java" % "5.1.30",
                    "com.typesafe.slick"  %% "slick" % "2.1.0",
                    "com.jolbox" % "bonecp" % "0.8.0.RELEASE",
                    "c3p0" % "c3p0" % "0.9.1.2"
                )
            )
        )
    lazy val learning = Project("learning", file("learning"),
            settings=defaultSettings ++ Seq(
                fork := true,
                libraryDependencies ++= Seq(
                    "com.typesafe.akka" %% "akka-actor" % "2.3.4"
                )
            )
        )
    lazy val root = Project("ceres", file("."), settings=defaultSettings).aggregate(platform, learning).dependsOn(platform, learning)
}
