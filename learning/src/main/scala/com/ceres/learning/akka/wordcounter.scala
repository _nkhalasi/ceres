package com.ceres.learning.akka

import akka.actor.{Actor, ActorSystem, ActorRef, Props}
import akka.event.Logging

case class ProcessStringMsg(string: String)
case class StringProcessedMsg(words: Integer)
 
class StringCounterActor extends Actor {
    val log = Logging(context.system, this)
    def receive = {
        case ProcessStringMsg(string) => {
            val wordsInLine = string.split(" ").length
            sender ! StringProcessedMsg(wordsInLine)
        }
        case _ => log.error("Error: message not recognized")
    }
}///:~

case class StartProcessFileMsg(filename: String)

class WordCounterActor extends Actor {
    private var result = 0
    private var totalLines = 0
    private var linesProcessed = 0
    private var fileSender:Option[ActorRef] = None
    def receive = {
        case StartProcessFileMsg(filename) => {
            fileSender = Some(sender)

            import akka.routing.RoundRobinPool
            var router = context.actorOf(RoundRobinPool(10).props(Props[StringCounterActor]), "sc")
            
            import scala.io.Source._
            fromFile(filename).getLines.foreach { line => 
                router ! ProcessStringMsg(line)
                totalLines += 1
            }
        }
        case StringProcessedMsg(words) => {
            result += words
            linesProcessed += 1
            if (totalLines == linesProcessed) {
                fileSender.map(_ ! result)
            }
        }
    }
}///:~

object WordCounter extends App {
    import akka.pattern.ask //enables ? operator/method
    import akka.util.Timeout //ask requires implicit timeout
    import scala.concurrent.duration._
    import akka.dispatch.ExecutionContexts._ //implicit ExecutionContext(global) required by closure in future.map
  
    override def main(args:Array[String]) = {
        implicit val ec = global
        implicit val timeout = Timeout(75 seconds)
        val system = ActorSystem("system")
        val counter = system.actorOf(Props[WordCounterActor])
        val future = counter ? StartProcessFileMsg(args(0))
        future.map { result =>
            println("Total number of words " + result)
            system.shutdown
        }
    }
}///:~