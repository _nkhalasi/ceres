package com.ceres.learning.futures

import scala.concurrent.{ Future, Promise, Await }
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

case class WordCount(count: Int)

object AsyncWordCounter {
  def countWords(source: String) = {
    val p = Promise[WordCount]()
    Future {
      p.success(WordCount(source.split(" ").length))
    }
    p.future
  }
}

object WordCounter {
  def countWords(filename: String) = {
    import scala.io.Source._
    val wordCounterFutures = fromFile(filename).getLines.map(line => AsyncWordCounter.countWords(line))
    val allFutures = Future.sequence(wordCounterFutures)
    allFutures onSuccess {
      case l => println(l.map(_.count).foldLeft(0)(_ + _))
    }
    allFutures onFailure {
      case l => 0
    }
    Await result (allFutures, 10 seconds)
  }
}

object WordCounterApp extends App {
  WordCounter.countWords(args(0))
}