package com.ceres.learning.futures

object WordCounter1 {
  def countWords(filename: String) = {
    import scala.io.Source._
    val wordCounts = fromFile(filename).getLines.map(line => line.split(" ").length)
    wordCounts.foldLeft(0)(_ + _)
  }
}

object ImperativeWordCounter extends App {
  println(WordCounter1.countWords(args(0)))
}